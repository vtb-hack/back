FROM openjdk:14
ADD target/back.jar back.jar

ENV KAFKA_BOOTSTRAP_SERVERS=localhost:9092\
    PG_USERNAME=postgres\
    PG_PASSWORD=root\
    PG_HOST=localhost:5434\
    PG_DATABASE=vtb

EXPOSE 80
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=prod", "back.jar"]
