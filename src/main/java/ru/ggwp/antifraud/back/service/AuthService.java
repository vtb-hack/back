package ru.ggwp.antifraud.back.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import ru.ggwp.antifraud.back.dto.ConfirmSmsChangePasswordDTO;
import ru.ggwp.antifraud.back.enumeration.AuthStatusEnum;
import ru.ggwp.antifraud.back.enumeration.SmsConfirmStatusEnum;
import ru.ggwp.antifraud.back.exception.NotFoundException;
import ru.ggwp.antifraud.back.exception.TooManyRequestsException;
import ru.ggwp.antifraud.back.jooq.tables.pojos.SmsConfirm;
import ru.ggwp.antifraud.back.jooq.tables.pojos.User;
import ru.ggwp.antifraud.back.dto.AuthDTO;
import ru.ggwp.antifraud.back.exception.BadRequestException;
import ru.ggwp.antifraud.back.repository.UsersRepository;
import ru.ggwp.antifraud.back.util.RequestClientUtil;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class AuthService {

    @Value("${antifraud.max-rejected-auth}")
    private Long maxRejectedAuth;

    private JwtService jwtService;
    private UsersRepository userRepository;
    private SmsConfirmService smsConfirmService;
    private AuthLogService authLogService;

    @Autowired
    public void AuthService(JwtService jwtService,
                            UsersRepository userRepository,
                            SmsConfirmService smsConfirmService,
                            AuthLogService authLogService) {
        this.jwtService = jwtService;
        this.userRepository = userRepository;
        this.smsConfirmService = smsConfirmService;
        this.authLogService = authLogService;
    }

    public String auth(AuthDTO authDTO, HttpServletRequest request, LocalDateTime now) throws BadRequestException {
        String ipAddress = request.getRemoteAddr();
        String browser = RequestClientUtil.getClientBrowser(request);
        String os = RequestClientUtil.getClientOS(request);
        String userAgent = RequestClientUtil.getUserAgent(request);

        Long rejectedAuth = authLogService.countByIpAndBadStatusWithin3Hours(ipAddress, now);

        if(maxRejectedAuth < rejectedAuth) {
            throw new TooManyRequestsException();
        }

        Optional<SmsConfirm> smsConfirm = smsConfirmService.findByLogin(authDTO.getLogin());

        if(smsConfirm.isPresent() && smsConfirm.get().getSmsConfirmStatus().equals(SmsConfirmStatusEnum.PENDING)) {
            authLogService.log(now, authDTO.getLogin(), ipAddress, browser, os, userAgent, AuthStatusEnum.USER_NEED_CONFIRM_SMS);
            return "NEED_CONFIRM";
        }

        Optional<User> user = userRepository.findByLogin(authDTO.getLogin());

        if(user.isEmpty()) {
            authLogService.log(now, authDTO.getLogin(), ipAddress, browser, os, userAgent, AuthStatusEnum.USER_NOT_FOUND);
            throw new NotFoundException();
        }

        if(!authDTO.getPassword().equals(user.get().getPassword())) {
            authLogService.log(now, authDTO.getLogin(), ipAddress, browser, os, userAgent, AuthStatusEnum.INCORRECT_PASSWORD);
            throw new BadRequestException();
        }

        authLogService.log(now, authDTO.getLogin(), ipAddress, browser, os, userAgent, AuthStatusEnum.SUCCESSFULLY);
        return jwtService.createToken(user.get(), request);
    }

    @Transactional
    public void changePasswordWithConfirmSms(ConfirmSmsChangePasswordDTO dto, LocalDateTime now) {
        smsConfirmService.confirm(dto.getLogin(), dto.getConfirm(), now);
        Optional<User> user = userRepository.findByLogin(dto.getLogin());

        if(user.isEmpty())
            throw new BadRequestException();

        user.get().setPassword(dto.getNewPassword());
        userRepository.save(user.get());
    }

}
