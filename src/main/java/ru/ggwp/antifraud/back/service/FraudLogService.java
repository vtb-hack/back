package ru.ggwp.antifraud.back.service;

import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.ggwp.antifraud.back.enumeration.FraudTypeEnum;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class FraudLogService {

    private JdbcTemplate jdbcTemplate;

    public void log(LocalDateTime timestamp, String ip, String browser, String os, String userAgent, FraudTypeEnum fraudTypeEnum) {
        jdbcTemplate.update(
        "INSERT INTO fraud_log (timestamp, ip_address, browser, os, user_agent, fraud_type) VALUES (?, ?, ?, ?, ?, ?)",
            new Object[] {
                timestamp,
                ip,
                browser,
                os,
                userAgent,
                fraudTypeEnum.name()
            }
        );
    }

}
