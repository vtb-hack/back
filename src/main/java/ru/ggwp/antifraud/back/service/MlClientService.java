package ru.ggwp.antifraud.back.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.ggwp.antifraud.back.model.MlBatchItemModel;
import ru.ggwp.antifraud.back.model.MlRequestModel;
import ru.ggwp.antifraud.back.model.MlResponseModel;

import java.util.List;

@Service
public class MlClientService {

    @Value("${ml-service.host}")
    private String HOST;

    private final RestTemplate restTemplate = new RestTemplate();

    public MlResponseModel checkFraud(List<MlBatchItemModel> mlBatchItemModels) {
        MlRequestModel mlRequestModel = new MlRequestModel(mlBatchItemModels);
        String baseUrl = HOST + "/batch";

        ResponseEntity<MlResponseModel> response = restTemplate.postForEntity(baseUrl,
                mlRequestModel,
                MlResponseModel.class
        );

        return response.getBody();
    }

}
