package ru.ggwp.antifraud.back.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;
import ru.ggwp.antifraud.back.dto.UserDTO;
import ru.ggwp.antifraud.back.exception.NotFoundException;
import ru.ggwp.antifraud.back.mapper.UserMapper;
import ru.ggwp.antifraud.back.repository.UsersRepository;

@Service
@Data
@AllArgsConstructor
public class UsersService {

    private UsersRepository userEntityRepository;
    private UserMapper userMapper;

    public UserDTO getUserDTO(String login) {
        return userMapper.entityToDto(
            userEntityRepository.findByLogin(login).orElseThrow(NotFoundException::new)
        );
    }

}
