package ru.ggwp.antifraud.back.service;

import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;

@Service
@AllArgsConstructor
public class PaymentLogService {

    private JdbcTemplate jdbcTemplate;

    public void log(Long          id,
                    LocalDateTime timestamp,
                    Long          accountNumber,
                    Long          bik,
                    Double        sum,
                    String        login,
                    String        ipAddress,
                    String        browser,
                    String        os,
                    String        userAgent) {
        jdbcTemplate.update(
        "INSERT INTO payment_log (id, timestamp, account_number, bik, sum, login, ip_address, browser, os, user_agent) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            new Object[] {
                id,
                timestamp,
                accountNumber,
                bik,
                sum,
                login,
                ipAddress,
                browser,
                os,
                userAgent
            }
        );
    }

}
