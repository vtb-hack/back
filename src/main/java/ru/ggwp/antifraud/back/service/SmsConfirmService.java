package ru.ggwp.antifraud.back.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.ggwp.antifraud.back.enumeration.SmsConfirmStatusEnum;
import ru.ggwp.antifraud.back.exception.BadRequestException;
import ru.ggwp.antifraud.back.jooq.tables.pojos.SmsConfirm;
import ru.ggwp.antifraud.back.repository.SmsConfirmRepository;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class SmsConfirmService {

    @Value("${sms-confirm.code")
    private String confirmCode;

    private SmsConfirmRepository smsConfirmRepository;

    SmsConfirmService(SmsConfirmRepository smsConfirmRepository) {
        this.smsConfirmRepository = smsConfirmRepository;
    }

    public void sendConfirm(String login) {
        Optional<SmsConfirm> oldConfirm = smsConfirmRepository.findFirstByLoginAndSmsConfirmStatusOrderByIdDesc(login, SmsConfirmStatusEnum.PENDING);

        if(oldConfirm.isPresent()) return;

        SmsConfirm smsConfirm = new SmsConfirm();
        smsConfirm.setLogin(login);
        smsConfirm.setSmsConfirmStatus(SmsConfirmStatusEnum.PENDING);
        smsConfirmRepository.save(smsConfirm);
    }

    public void confirm(String login, String confirmCode, LocalDateTime now) {
        if(!confirmCode.equals(confirmCode))
            throw new BadRequestException();

        Optional<SmsConfirm> smsConfirm = smsConfirmRepository.findFirstByLoginAndSmsConfirmStatusOrderByIdDesc(login, SmsConfirmStatusEnum.PENDING);

        if(smsConfirm.isEmpty())
            throw new BadRequestException();

        smsConfirm.get().setSmsConfirmStatus(SmsConfirmStatusEnum.DONE);
        smsConfirm.get().setConfirmDate(now);

        smsConfirmRepository.save(smsConfirm.get());
    }

    public Optional<SmsConfirm> findByLogin(String login) {
        return smsConfirmRepository.findFirstByLoginOrderByIdDesc(login);
    }

}
