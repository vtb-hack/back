package ru.ggwp.antifraud.back.service;

import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.ggwp.antifraud.back.enumeration.AuthStatusEnum;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class AuthLogService {

    private JdbcTemplate jdbcTemplate;

    public void log(LocalDateTime timestamp, String login, String ip, String browser, String os, String userAgent, AuthStatusEnum authStatusEnum) {
        jdbcTemplate.update(
                "INSERT INTO auth_log (timestamp, login, ip_address, browser, os, user_agent, auth_status) VALUES (?, ?, ?, ?, ?, ?, ?)",
                new Object[] {
                        timestamp,
                        login,
                        ip,
                        browser,
                        os,
                        userAgent,
                        authStatusEnum.name()
                }
        );
    }

    public Long countByIpAndBadStatusWithin3Hours(String ip, LocalDateTime now) {
        LocalDateTime from = now.minusHours(3);
        Long count = jdbcTemplate.queryForObject("SELECT count(*) FROM auth_log " +
                        "WHERE ip_address = ? AND auth_status <> ? AND timestamp BETWEEN ? AND ?",
                        new Object[] {
                            ip,
                            AuthStatusEnum.SUCCESSFULLY.name(),
                            from,
                            now
                        },
                        Long.class);
        return count;
    }

}
