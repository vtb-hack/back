package ru.ggwp.antifraud.back.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ggwp.antifraud.back.dto.PaymentDTO;
import ru.ggwp.antifraud.back.enumeration.FraudTypeEnum;
import ru.ggwp.antifraud.back.exception.AuthorizationException;
import ru.ggwp.antifraud.back.exception.NotFoundException;
import ru.ggwp.antifraud.back.jooq.tables.pojos.Payment;
import ru.ggwp.antifraud.back.model.MlBatchItemModel;
import ru.ggwp.antifraud.back.model.MlResponseModel;
import ru.ggwp.antifraud.back.repository.PaymentRepository;
import ru.ggwp.antifraud.back.util.AuthHeaderUtil;
import ru.ggwp.antifraud.back.util.RequestClientUtil;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class PaymentService {

    private PaymentRepository paymentRepository;
    private PaymentLogService paymentLogService;
    private FraudLogService fraudLogService;
    private CursorLogService cursorLogService;
    private SmsConfirmService smsConfirmService;
    private MlClientService mlClientService;

    @Transactional
    public Long payment(PaymentDTO paymentDTO, LocalDateTime timestamp, String login, HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");
        String jwt = AuthHeaderUtil.getClearToken(authHeader);
        Long count = cursorLogService.countCursorLogsByJwt(jwt);

        Long accountNumber = paymentDTO.getAccountNumber();
        Long bik = paymentDTO.getBik();
        Double sum = paymentDTO.getSum();

        String ip = request.getRemoteAddr();
        String browser = RequestClientUtil.getClientBrowser(request);
        String os =  RequestClientUtil.getClientOS(request);
        String userAgent = RequestClientUtil.getUserAgent(request);

        if(count == 0) {
            smsConfirmService.sendConfirm(login);
            fraudLogService.log(timestamp, ip, browser, os, userAgent, FraudTypeEnum.BOT);
            throw new AuthorizationException();
        }

        List<MlBatchItemModel> mlBatchItemModels = cursorLogService.getMlBatchItems(jwt);

        MlResponseModel mlResponseModel = mlClientService.checkFraud(mlBatchItemModels);

        if(mlResponseModel.getPred() < 0.39) {
            smsConfirmService.sendConfirm(login);
            fraudLogService.log(timestamp, ip, browser, os, userAgent, FraudTypeEnum.CURSOR_ACTIVITY);
            throw new AuthorizationException();
        }

        Payment payment = new Payment(null, accountNumber, bik, sum, login);
        Payment finalPayment =  paymentRepository.save(payment);

        paymentLogService.log(finalPayment.getId(), timestamp, accountNumber, bik, sum, login, ip, browser, os, userAgent);

        return finalPayment.getId();
    }

    public PaymentDTO getForUser(Long id, String login) {
        Payment payment = paymentRepository.findByIdAndLogin(id, login)
                .orElseThrow(NotFoundException::new);
        return new PaymentDTO(payment.getAccountNumber(), payment.getBik(), payment.getSum());
    }

}
