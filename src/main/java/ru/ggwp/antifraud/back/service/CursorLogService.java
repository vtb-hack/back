package ru.ggwp.antifraud.back.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.ggwp.antifraud.back.dto.CursorLogDTO;
import ru.ggwp.antifraud.back.listener.event.CursorLogEvent;
import ru.ggwp.antifraud.back.model.MlBatchItemModel;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CursorLogService {

    private KafkaTemplate<String, String> kafkaTemplate;
    private JdbcTemplate jdbcTemplate;

    public CursorLogService(KafkaTemplate<String, String> kafkaTemplate,
                            JdbcTemplate jdbcTemplate) {
        this.kafkaTemplate = kafkaTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    public String convertLogToJson(CursorLogDTO cursorLogDTO, String login, String jwt) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        CursorLogEvent cursorLogEvent = new CursorLogEvent();

        cursorLogEvent.setTimestamp(cursorLogDTO.getTimestamp().getTime());
        cursorLogEvent.setLogin(login);
        cursorLogEvent.setJwt(jwt);
        cursorLogEvent.setX(cursorLogDTO.getX());
        cursorLogEvent.setY(cursorLogDTO.getY());
        cursorLogEvent.setButton(cursorLogDTO.getButton());
        cursorLogEvent.setState(cursorLogDTO.getState());

        return mapper.writeValueAsString(cursorLogEvent);
    }

    public Object[] convertLogToObjectRow(String log) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        CursorLogEvent event = mapper.readValue(log, CursorLogEvent.class);
        Object[] row = new Object[7];

        row[0] = new Timestamp(event.getTimestamp());
        row[1] = event.getLogin();
        row[2] = event.getJwt();
        row[3] = event.getX();
        row[4] = event.getY();
        row[5] = event.getButton().name();
        row[6] = event.getState().name();

        return row;
    }

    public List<Object[]> convertLogToObjectRow(List<String> logs) throws JsonProcessingException {
        List<Object[]> rows = new ArrayList<>();

        for(String log: logs) {
            Object[] row = convertLogToObjectRow(log);
            rows.add(row);
        }

        return rows;
    }

    public void log(CursorLogDTO cursorLogDTO, String login, String jwt) throws JsonProcessingException {
        String logString = convertLogToJson(cursorLogDTO, login, jwt);
        kafkaTemplate.send("CURSOR_LOG_TOPIC-3", logString);
    }

    public void log(List<CursorLogDTO> cursorLogDTOList, String login, String jwt) throws JsonProcessingException {
        for(CursorLogDTO cursorLogDTO: cursorLogDTOList) {
            log(cursorLogDTO, login, jwt);
        }
    }

    public void saveLog(List<String> logs) throws JsonProcessingException {
        List<Object[]> rows = convertLogToObjectRow(logs);
        jdbcTemplate.batchUpdate("INSERT INTO cursor_log (timestamp, login, jwt, x, y, button, state) VALUES(?, ?, ?, ?, ?, ?, ?)", rows);
    }

    public Long countCursorLogsByJwt(String jwt) {
        return jdbcTemplate.queryForObject("SELECT count(*) FROM cursor_log WHERE jwt = ?", new Object[]{jwt}, Long.class);
    }

    public List<MlBatchItemModel> getMlBatchItems(String jwt) {
        List<Map<String, Object>> list = jdbcTemplate.queryForList("SELECT time_bucket('10 milliseconds', timestamp) datetime, avg(x)::BIGINT x, avg(y)::BIGINT y, button, state " +
            "FROM cursor_log WHERE jwt = ? " +
            "GROUP BY datetime, button, state " +
            "ORDER BY datetime ASC " +
            "LIMIT 200",
            new Object[] {jwt}
        );

        if(list.size() == 0)
            return new ArrayList<>();

        Timestamp startDate = (Timestamp) list.get(0).get("datetime");

        List<MlBatchItemModel> mapped = list.stream().map(item -> {
            Timestamp timestamp = (Timestamp) item.get("datetime");
            Long x = (Long) item.get("x");
            Long y = (Long) item.get("y");
            String button = (String) item.get("button");
            String state = (String) item.get("state");

            BigDecimal logSecond = BigDecimal.valueOf(timestamp.getTime() - startDate.getTime())
                    .divide(BigDecimal.valueOf(1000));

            return new MlBatchItemModel(
                logSecond.doubleValue(),
                button,
                state,
                x,
                y
            );
        }).collect(Collectors.toList());

        return mapped;
    }

//    public void checkFraud(List<String> logs) {
//        List<Object[]> rows = convertLogToObjectRow(logs);
//
//        rows = rows.stream().sorted((a, b) -> {
//            return ((Timestamp) a[TIMESTAMP_COLUMN_INDEX]).compareTo(
//                    (Timestamp) b[TIMESTAMP_COLUMN_INDEX]);
//        }).collect(Collectors.toList());
//
//
//    }

}
