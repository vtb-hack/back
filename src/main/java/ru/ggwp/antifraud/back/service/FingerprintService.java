package ru.ggwp.antifraud.back.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.ggwp.antifraud.back.util.RequestClientUtil;

import javax.servlet.http.HttpServletRequest;

@Service
public class FingerprintService {

    @Value("${fingerprint.separator}")
    private String SEPARATOR;

    private Pbkdf2PasswordEncoder pbkdf2PasswordEncoder;

    @Autowired
    public FingerprintService(Pbkdf2PasswordEncoder pbkdf2PasswordEncoder) {
        this.pbkdf2PasswordEncoder = pbkdf2PasswordEncoder;
    }

    public String createPrint(HttpServletRequest request) {
        String data = getConcatData(request);
        return pbkdf2PasswordEncoder.encode(data);
    }

    public Boolean matchPrint(HttpServletRequest request, String fingerprint) {
        String data = getConcatData(request);
        return pbkdf2PasswordEncoder.matches(data, fingerprint);
    }

    private String getConcatData(HttpServletRequest request) {
        String userAgent = RequestClientUtil.getUserAgent(request);
        String clientBrowser = RequestClientUtil.getClientBrowser(request);
        String clientOs = RequestClientUtil.getClientOS(request);

        String[] row = new String[] {
            userAgent,
            clientBrowser,
            clientOs
        };

        return String.join(SEPARATOR, row);
    }

}
