package ru.ggwp.antifraud.back.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.ggwp.antifraud.back.jooq.tables.pojos.User;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

@Service
public class JwtService {

    @Value("${spring.security.jwt.secret}")
    private String SECRET;

    @Value("${spring.security.jwt.expires}")
    private Long EXPIRES;

    private Algorithm algorithm;

    private FingerprintService fingerprintService;

    @Autowired
    public JwtService(FingerprintService fingerprintService) {
        this.fingerprintService = fingerprintService;
    }

    @PostConstruct
    public void setUp() {
        algorithm = Algorithm.HMAC256(SECRET);
    }

    public String createToken(User user, HttpServletRequest request) throws JWTCreationException {
        LocalDateTime now = LocalDateTime.now().now();
        LocalDateTime expires = now.plusSeconds(EXPIRES);
        String fingerprint = fingerprintService.createPrint(request);
        return JWT.create()
            .withClaim("created", Timestamp.valueOf(now).getTime())
            .withClaim("login", user.getLogin())
            .withClaim("email", user.getEmail())
            .withClaim("role", user.getRole().name())
            .withClaim("fingerprint", fingerprint)
            .withExpiresAt(Date.from(expires.toInstant(ZoneOffset.UTC)))
            .sign(algorithm);
    }

    public DecodedJWT expiresAt(String token) throws JWTVerificationException {
        JWTVerifier verifier = JWT.require(algorithm)
                .build();
        return verifier.verify(token);
    }

}
