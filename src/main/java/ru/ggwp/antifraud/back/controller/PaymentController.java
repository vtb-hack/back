package ru.ggwp.antifraud.back.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ggwp.antifraud.back.dto.PaymentDTO;
import ru.ggwp.antifraud.back.service.PaymentService;
import ru.ggwp.antifraud.back.util.SecurityUtil;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@RestController
@AllArgsConstructor
@RequestMapping("/api/payments")
public class PaymentController {

    private PaymentService paymentService;

    @PostMapping
    public ResponseEntity<Long> payment(@RequestBody PaymentDTO paymentDTO, HttpServletRequest request) {
        LocalDateTime timestamp = LocalDateTime.now();
        String login = SecurityUtil.getUserLogin();
        return ResponseEntity.ok(paymentService.payment(paymentDTO, timestamp, login, request));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PaymentDTO> getPayment(@PathVariable Long id) {
        String login = SecurityUtil.getUserLogin();
        return ResponseEntity.ok(paymentService.getForUser(id, login));
    }

}
