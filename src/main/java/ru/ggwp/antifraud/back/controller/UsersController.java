package ru.ggwp.antifraud.back.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ggwp.antifraud.back.dto.UserDTO;
import ru.ggwp.antifraud.back.service.UsersService;
import ru.ggwp.antifraud.back.util.SecurityUtil;

@AllArgsConstructor
@RestController
@RequestMapping("/api/users")
public class UsersController {

    private UsersService usersService;

    @GetMapping("/current")
    public ResponseEntity<UserDTO> getCurrentUser() {
        return ResponseEntity.ok(usersService.getUserDTO(SecurityUtil.getUserLogin()));
    }

}
