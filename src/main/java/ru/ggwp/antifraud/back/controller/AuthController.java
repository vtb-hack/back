package ru.ggwp.antifraud.back.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.ggwp.antifraud.back.dto.AuthDTO;
import ru.ggwp.antifraud.back.dto.ConfirmSmsChangePasswordDTO;
import ru.ggwp.antifraud.back.exception.BadRequestException;
import ru.ggwp.antifraud.back.service.AuthService;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@RestController
@AllArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {

    private AuthService authService;

    @PostMapping("/login")
    public String auth(@RequestBody AuthDTO authDTO, HttpServletRequest request) throws BadRequestException {
        LocalDateTime now = LocalDateTime.now();
        return authService.auth(authDTO, request, now);
    }

    @PostMapping("/confirm-sms/change-password")
    public void changePasswordWithConfirmSms(@RequestBody ConfirmSmsChangePasswordDTO dto) throws BadRequestException {
        LocalDateTime now = LocalDateTime.now();
        authService.changePasswordWithConfirmSms(dto, now);
    }


}
