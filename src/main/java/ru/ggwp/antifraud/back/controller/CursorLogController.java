package ru.ggwp.antifraud.back.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ggwp.antifraud.back.dto.CursorLogBachDTO;
import ru.ggwp.antifraud.back.dto.CursorLogDTO;
import ru.ggwp.antifraud.back.service.CursorLogService;
import ru.ggwp.antifraud.back.util.AuthHeaderUtil;
import ru.ggwp.antifraud.back.util.SecurityUtil;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.Instant;

@RestController
@AllArgsConstructor
@RequestMapping("/api/cursor-log")
public class CursorLogController {

    private CursorLogService cursorLogService;

    @PostMapping("/batch")
    public void log(@RequestBody CursorLogBachDTO cursorLogBachDTO, HttpServletRequest httpServletRequest) throws JsonProcessingException {
        String login = SecurityUtil.getUserLogin();
        String authHeader = httpServletRequest.getHeader("Authorization");
        String jwt = AuthHeaderUtil.getClearToken(authHeader);
        cursorLogService.log(cursorLogBachDTO.getBatch(), login, jwt);
    }

}
