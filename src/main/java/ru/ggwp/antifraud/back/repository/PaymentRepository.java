package ru.ggwp.antifraud.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ggwp.antifraud.back.jooq.tables.pojos.Payment;

import java.util.Optional;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
    Optional<Payment> findByIdAndLogin(Long id, String login);
}
