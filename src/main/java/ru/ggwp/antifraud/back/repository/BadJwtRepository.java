package ru.ggwp.antifraud.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ggwp.antifraud.back.jooq.tables.pojos.BadJwt;

public interface BadJwtRepository extends JpaRepository<BadJwt, String> {
}
