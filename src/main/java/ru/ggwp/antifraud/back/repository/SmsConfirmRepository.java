package ru.ggwp.antifraud.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ggwp.antifraud.back.enumeration.SmsConfirmStatusEnum;
import ru.ggwp.antifraud.back.jooq.tables.pojos.SmsConfirm;

import java.util.Optional;

@Repository
public interface SmsConfirmRepository extends JpaRepository<SmsConfirm, Long> {
    Optional<SmsConfirm> findFirstByLoginAndSmsConfirmStatusOrderByIdDesc(String login, SmsConfirmStatusEnum status);
    Optional<SmsConfirm> findFirstByLoginOrderByIdDesc(String login);
}
