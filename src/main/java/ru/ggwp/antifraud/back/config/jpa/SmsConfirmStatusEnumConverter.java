package ru.ggwp.antifraud.back.config.jpa;

import ru.ggwp.antifraud.back.enumeration.SmsConfirmStatusEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class SmsConfirmStatusEnumConverter implements AttributeConverter<SmsConfirmStatusEnum, String> {

    @Override
    public String convertToDatabaseColumn(SmsConfirmStatusEnum enumeration) {
        if (enumeration == null) {
            return null;
        }
        return enumeration.name();
    }

    @Override
    public SmsConfirmStatusEnum convertToEntityAttribute(String enumeration) {
        if (enumeration == null) {
            return null;
        }

        return Stream.of(SmsConfirmStatusEnum.values())
                .filter(c -> c.name().equals(enumeration))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
