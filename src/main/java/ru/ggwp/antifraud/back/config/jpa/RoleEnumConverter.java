package ru.ggwp.antifraud.back.config.jpa;

import ru.ggwp.antifraud.back.enumeration.RolesEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class RoleEnumConverter implements AttributeConverter<RolesEnum, String> {

    @Override
    public String convertToDatabaseColumn(RolesEnum enumeration) {
        if (enumeration == null) {
            return null;
        }
        return enumeration.name();
    }

    @Override
    public RolesEnum convertToEntityAttribute(String enumeration) {
        if (enumeration == null) {
            return null;
        }

        return Stream.of(RolesEnum.values())
                .filter(c -> c.name().equals(enumeration))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
