package ru.ggwp.antifraud.back.config.jpa;

import ru.ggwp.antifraud.back.enumeration.FraudTypeEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class FraudTypeEnumConverter implements AttributeConverter<FraudTypeEnum, String> {

    @Override
    public String convertToDatabaseColumn(FraudTypeEnum enumeration) {
        if (enumeration == null) {
            return null;
        }
        return enumeration.name();
    }

    @Override
    public FraudTypeEnum convertToEntityAttribute(String enumeration) {
        if (enumeration == null) {
            return null;
        }

        return Stream.of(FraudTypeEnum.values())
                .filter(c -> c.name().equals(enumeration))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
