package ru.ggwp.antifraud.back.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.ggwp.antifraud.back.dto.JwtPayloadDTO;
import ru.ggwp.antifraud.back.enumeration.RolesEnum;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Component
public class SecurityUtil {

    public static String getUserLogin() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    public static boolean containsRole(RolesEnum[] roles) {
        List<RolesEnum> authorities = Arrays.asList(getUserRoles());
        for(RolesEnum role: roles) {
            if(authorities.contains(role)) return true;
        }
        return false;
    }

    public static boolean containsRole(RolesEnum role) {
        return containsRole(new RolesEnum[]{role});
    }

    public static RolesEnum[] getUserRoles() {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        return authorities.stream().map(item -> RolesEnum.valueOf(item.getAuthority())).toArray(RolesEnum[]::new);
    }

    public static JwtPayloadDTO getUserDetails() {
        return (JwtPayloadDTO) SecurityContextHolder.getContext().getAuthentication().getDetails();
    }

    public static boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && authentication.isAuthenticated();
    }

}
