package ru.ggwp.antifraud.back.util;

public class AuthHeaderUtil {

    public static final String BEARER = "Bearer ";

    public static String getClearToken(String authHeader) {
        String token = authHeader != null && authHeader.indexOf(BEARER) != -1 ? authHeader : null;
        token = token != null ? token.substring(BEARER.length()) : null;
        return token;
    }

}
