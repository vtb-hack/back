package ru.ggwp.antifraud.back.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import ru.ggwp.antifraud.back.service.CursorLogService;

import java.util.List;

@Component
@AllArgsConstructor
public class CursorLogListener {

    private CursorLogService cursorLogService;

    @KafkaListener(topics = "CURSOR_LOG_TOPIC-3")
    public void onMoveCursor(@Payload List<String> cursorLogs) throws JsonProcessingException {
        cursorLogService.saveLog(cursorLogs);
    }

//    @KafkaListener(topics = "CURSOR_LOG_FRAUD_CHECK")
//    public void onMoveCursor(@Payload List<String> cursorLogs) {
//        cursorLogService.saveLog(cursorLogs);
//    }

}
