package ru.ggwp.antifraud.back.listener.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ggwp.antifraud.back.enumeration.CursorLogButtonEnum;
import ru.ggwp.antifraud.back.enumeration.CursorLogStateEnum;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CursorLogEvent {
    private Long timestamp;
    private String login;
    private String jwt;
    private Long x;
    private Long y;
    private CursorLogButtonEnum button;
    private CursorLogStateEnum state;
}
