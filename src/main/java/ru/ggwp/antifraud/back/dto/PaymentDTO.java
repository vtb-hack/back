package ru.ggwp.antifraud.back.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentDTO {
    private Long          accountNumber;
    private Long          bik;
    private Double        sum;
}
