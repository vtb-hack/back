package ru.ggwp.antifraud.back.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ggwp.antifraud.back.enumeration.RolesEnum;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private Long   id;
    private String login;
    private String email;
    private String firstName;
    private String lastName;
    private RolesEnum role;
}
