package ru.ggwp.antifraud.back.dto;

import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class JwtPayloadDTO {
    private String login;
    private String email;
    private String role;
    private String fingerprint;

    public JwtPayloadDTO(DecodedJWT decodedJWT) {
        this.login = decodedJWT.getClaim("login").asString();
        this.email = decodedJWT.getClaim("email").asString();
        this.role = decodedJWT.getClaim("role").asString();
        this.fingerprint = decodedJWT.getClaim("fingerprint").asString();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }
}
