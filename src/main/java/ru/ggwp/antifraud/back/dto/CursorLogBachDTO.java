package ru.ggwp.antifraud.back.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CursorLogBachDTO {
    private List<CursorLogDTO> batch;
}
