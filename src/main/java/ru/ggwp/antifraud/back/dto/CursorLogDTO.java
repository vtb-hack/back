package ru.ggwp.antifraud.back.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ggwp.antifraud.back.enumeration.CursorLogButtonEnum;
import ru.ggwp.antifraud.back.enumeration.CursorLogStateEnum;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CursorLogDTO {
    private Timestamp timestamp;
    private Long x;
    private Long y;
    private CursorLogButtonEnum button;
    private CursorLogStateEnum state;
}
