package ru.ggwp.antifraud.back.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfirmSmsChangePasswordDTO {
    private String confirm;
    private String login;
    private String newPassword;
}
