package ru.ggwp.antifraud.back.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MlBatchItemModel {
    private Double t;
    private String button;
    private String state;
    private Long x;
    private Long y;
}
