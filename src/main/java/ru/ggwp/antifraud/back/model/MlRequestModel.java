package ru.ggwp.antifraud.back.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class MlRequestModel {
    List<MlBatchItemModel> batch;
}
