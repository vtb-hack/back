package ru.ggwp.antifraud.back.mapper;

import org.mapstruct.Mapper;
import ru.ggwp.antifraud.back.jooq.tables.pojos.User;
import ru.ggwp.antifraud.back.dto.UserDTO;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDTO entityToDto(User entity);

}
