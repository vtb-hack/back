package ru.ggwp.antifraud.back.enumeration;

public enum AuthStatusEnum {
    SUCCESSFULLY,
    USER_NOT_FOUND,
    INCORRECT_PASSWORD,
    USER_NEED_CONFIRM_SMS
}
