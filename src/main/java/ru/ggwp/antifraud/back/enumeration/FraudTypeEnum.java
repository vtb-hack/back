package ru.ggwp.antifraud.back.enumeration;

public enum FraudTypeEnum {
    TOKEN_THEFT,
    BOT,
    CURSOR_ACTIVITY
}
