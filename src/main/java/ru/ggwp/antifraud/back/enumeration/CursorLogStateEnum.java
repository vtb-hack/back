package ru.ggwp.antifraud.back.enumeration;

public enum CursorLogStateEnum {
    Move,
    Pressed,
    Released
}
