package ru.ggwp.antifraud.back.enumeration;

public enum CursorLogButtonEnum {
    NoButton,
    Left,
    Right;
}
