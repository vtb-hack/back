package ru.ggwp.antifraud.back.enumeration;

public enum SmsConfirmStatusEnum {
    PENDING,
    DONE
}
