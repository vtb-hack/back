package ru.ggwp.antifraud.back.filter;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.AllArgsConstructor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Component;
import ru.ggwp.antifraud.back.dto.JwtPayloadDTO;
import ru.ggwp.antifraud.back.enumeration.FraudTypeEnum;
import ru.ggwp.antifraud.back.enumeration.SmsConfirmStatusEnum;
import ru.ggwp.antifraud.back.jooq.tables.pojos.SmsConfirm;
import ru.ggwp.antifraud.back.repository.BadJwtRepository;
import ru.ggwp.antifraud.back.service.FingerprintService;
import ru.ggwp.antifraud.back.service.FraudLogService;
import ru.ggwp.antifraud.back.service.JwtService;
import ru.ggwp.antifraud.back.service.SmsConfirmService;
import ru.ggwp.antifraud.back.util.AuthHeaderUtil;
import ru.ggwp.antifraud.back.util.RequestClientUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

@Component
@AllArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SecurityFilter implements Filter {

    private JwtService jwtService;
    private FingerprintService fingerprintService;
    private BadJwtRepository badJwtRepository;
    private FraudLogService fraudLogService;
    private SmsConfirmService confirmService;

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String authHeader = request.getHeader("Authorization");
        String token = AuthHeaderUtil.getClearToken(authHeader);
        DecodedJWT decodedJWT = null;

        if(token != null) {
            try {
                decodedJWT = jwtService.expiresAt(token);
            } catch (JWTVerificationException e) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }

        if(decodedJWT != null) {
            String fingerprint = decodedJWT.getClaim("fingerprint").asString();
            Boolean matchFingerprint = fingerprintService.matchPrint(request, fingerprint);
            if(!matchFingerprint) {
                confirmService.sendConfirm(decodedJWT.getClaim("login").asString());
                fraudLogService.log(
                        LocalDateTime.now(),
                        request.getRemoteAddr(),
                        RequestClientUtil.getClientBrowser(request),
                        RequestClientUtil.getClientOS(request),
                        RequestClientUtil.getUserAgent(request),
                        FraudTypeEnum.TOKEN_THEFT
                );
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }

        if(decodedJWT != null) {
            String login = decodedJWT.getClaim("login").asString();
            Optional<SmsConfirm> smsConfirm = confirmService.findByLogin(login);

            if(smsConfirm.isPresent()) {
                Boolean needConfirm = smsConfirm.get().getSmsConfirmStatus().equals(SmsConfirmStatusEnum.PENDING);

                if(needConfirm) {
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    return;
                } else {
                    if(Timestamp.valueOf(smsConfirm.get().getConfirmDate()).after(new Timestamp(decodedJWT.getClaim("created").asLong()))) {
                        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                        return;
                    }
                }
            }
        }

        if(decodedJWT != null) {
            JwtPayloadDTO payload = new JwtPayloadDTO(decodedJWT);

            Authentication authentication = new Authentication() {
                @Override
                public Collection<? extends GrantedAuthority> getAuthorities() {
                    return Arrays.asList(new GrantedAuthority() {
                        @Override
                        public String getAuthority() {
                            return payload.getRole();
                        }
                    });
                }

                @Override
                public Object getCredentials() {
                    return null;
                }

                @Override
                public Object getDetails() {
                    return payload;
                }

                @Override
                public Object getPrincipal() {
                    return payload.getLogin();
                }

                @Override
                public boolean isAuthenticated() {
                    return true;
                }

                @Override
                public void setAuthenticated(boolean b) throws IllegalArgumentException {

                }

                @Override
                public String getName() {
                    return payload.getLogin();
                }
            };

            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(authentication);
            HttpSession session = request.getSession(true);
            session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);
        } else {
            SecurityContext securityContext = SecurityContextHolder.getContext();
            SecurityContextHolder.getContext().setAuthentication(null);
            HttpSession session = request.getSession(true);
            session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);
        }

        chain.doFilter(req, res);
    }

}
