CREATE TABLE cursor_log (
    timestamp               TIMESTAMP NOT NULL,
    login                   VARCHAR NOT NULL,
    jwt                     VARCHAR NOT NULL,
    x                       BIGINT NOT NULL,
    y                       BIGINT NOT NULL,
    button                  VARCHAR NOT NULL,
    state                   VARCHAR NOT NULL
);

CREATE INDEX ON cursor_log (jwt, timestamp DESC);

-- SELECT create_hypertable(
--   'cursor_log',
--   'timestamp',
--   chunk_time_interval => interval '1 day'
-- );