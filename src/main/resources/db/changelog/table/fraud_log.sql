CREATE TABLE fraud_log (
    timestamp       TIMESTAMP NOT NULL,
    ip_address      VARCHAR NOT NULL,
    browser         VARCHAR NOT NULL,
    os              VARCHAR NOT NULL,
    user_agent      VARCHAR NOT NULL,
    fraud_type      VARCHAR NOT NULL
);

CREATE INDEX ON fraud_log (ip_address, timestamp DESC);

-- SELECT create_hypertable(
--   'fraud_log',
--   'timestamp',
--   chunk_time_interval => interval '7 days'
-- );