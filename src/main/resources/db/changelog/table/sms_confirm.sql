CREATE TABLE sms_confirm (
    id                      BIGSERIAL PRIMARY KEY,
    login                   VARCHAR,
    sms_confirm_status      VARCHAR NOT NULL,
    confirm_date            TIMESTAMP,

    FOREIGN KEY (login) REFERENCES users (login)
)