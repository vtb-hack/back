CREATE TABLE payment_log (
    id                  BIGINT NOT NULL,
    timestamp           TIMESTAMP NOT NULL,
    account_number      BIGINT NOT NULL,
    bik                 BIGINT NOT NULL,
    sum                 DOUBLE PRECISION NOT NULL,
    login               VARCHAR NOT NULL,

    ip_address          VARCHAR NOT NULL,
    browser             VARCHAR NOT NULL,
    os                  VARCHAR NOT NULL,
    user_agent          VARCHAR NOT NULL
);

CREATE INDEX ON payment_log (id, login, timestamp DESC);

-- SELECT create_hypertable(
--   'payment_log',
--   'timestamp',
--   chunk_time_interval => interval '30 days'
-- );