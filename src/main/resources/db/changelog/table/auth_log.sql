CREATE TABLE auth_log (
    timestamp       TIMESTAMP NOT NULL,
    login           VARCHAR NOT NULL,
    ip_address      VARCHAR NOT NULL,
    browser         VARCHAR NOT NULL,
    os              VARCHAR NOT NULL,
    user_agent      VARCHAR NOT NULL,
    auth_status     VARCHAR NOT NULL

);

CREATE INDEX ON auth_log (ip_address, timestamp DESC);

-- SELECT create_hypertable(
--   'auth_log',
--   'timestamp',
--   chunk_time_interval => interval '7 days'
-- );