CREATE TABLE payments (
    id                  BIGSERIAL PRIMARY KEY,
    account_number      BIGINT NOT NULL,
    bik                 BIGINT NOT NULL,
    sum                 DOUBLE PRECISION NOT NULL,
    login               VARCHAR,

    FOREIGN KEY (login) REFERENCES users (login)
);