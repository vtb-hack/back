CREATE TABLE users (
    id BIGSERIAL    PRIMARY KEY,
    login           VARCHAR UNIQUE,
    password        VARCHAR,
    email           VARCHAR UNIQUE,
    first_name      VARCHAR,
    last_name       VARCHAR,
    role            VARCHAR
);